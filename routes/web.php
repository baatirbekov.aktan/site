<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [\App\Http\Controllers\FormController::class, 'index']);

Route::get('/forms', [\App\Http\Controllers\FormController::class, 'index'])->name('forms.index');

Route::get('/forms/{form}', [\App\Http\Controllers\FormController::class, 'show'])->name('forms.show');

Route::get('/answers', [\App\Http\Controllers\AnswerController::class, 'index'])->name('answers.index');

Route::get('/forms/fill/{form}', [\App\Http\Controllers\FormController::class, 'fill'])->name('forms.fill');

Route::post('/forms', [\App\Http\Controllers\FormController::class, 'store'])->name('forms.store');
