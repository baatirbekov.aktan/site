<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Services\JsonRpcClient;

class AnswerController extends Controller
{
    protected JsonRpcClient $client;

    /**
     * @param JsonRpcClient $client
     */
    public function __construct(JsonRpcClient $client)
    {
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     * @throws GuzzleException
     */
    public function index(): View|Factory|Application
    {
        $data = $this->client->send('answers','answers@index');
        $answers = $data['result'];

        return view('answers.index', compact('answers'));
    }
}
