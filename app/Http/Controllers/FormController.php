<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormsRequest;
use App\Services\JsonRpcClient;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class FormController extends Controller
{
    protected JsonRpcClient $client;

    /**
     * @param JsonRpcClient $client
     */
    public function __construct(JsonRpcClient $client)
    {
        $this->client = $client;
    }

    /**
     * @return Factory|View|Application
     * @throws GuzzleException
     */
    public function index(): Factory|View|Application
    {
        $data = $this->client->send('forms', 'forms@index');
        $forms = $data['result'];

        return view('forms.index', compact('forms'));
    }

    public function show($form)
    {
        try {
            $data = $this->client->send('forms', 'forms@answers', ['form_id' => $form]);
            $answers = $data['result'];
        } catch (\Throwable $exception) {
            return redirect()->route('forms.index')->with('error', 'There is no form like this');
        }
        return view('forms.show', compact('answers'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param $form
     * @return Application|Factory|View
     * @throws GuzzleException
     */
    public function fill($form): View|Factory|Application
    {
        $data = $this->client->send('forms', 'forms@show', ['form_id' => $form]);
        $form = $data['result'];

        return view('forms.fill', compact('form'));
    }

    /**
     * Display a listing of the resource.
     *
     * @param FormsRequest $request
     * @return RedirectResponse
     * @throws GuzzleException
     */
    public function store(FormsRequest $request): RedirectResponse
    {
        $this->client->send('answers', 'answers@store', ['data' => $request->all()]);

        return redirect()->route('forms.index')->with('success', 'Successfully filled');
    }
}
