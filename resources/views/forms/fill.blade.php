@extends('layouts.app')

@section('content')

    <div>
        <h3>{{ $form['name'] }}</h3>
    </div>

    <div class="w-50">
        <h3>Fields</h3>
        <form action="{{ route('forms.store') }}" method="post">
            @csrf
            @error('answers.*')
            <p class="text-danger">{{$message}}</p>
            @enderror
            <input type="hidden" value="{{ $form['id'] }}" name="form_id">
            @foreach($form['selects'] as $select)
                <div class="my-2">
                    <label for="{{ $select['name'] }}" class="form-label">{{ $select['name'] }}</label>
                    <select name="answers[{{ $select['name'] }}]" required class="form-select"
                            id="{{ $select['name'] }}">
                        @foreach($select['variants'] as $variant)
                            <option value="{{ $variant['variant'] }}">{{ $variant['variant'] }}</option>
                        @endforeach
                    </select>
                    <div class="form-text">{{ $select['description'] }}</div>
                </div>
            @endforeach

            @foreach($form['textareas'] as $textarea)
                <div class="my-2">
                    <label for="{{ $textarea['name'] }}" class="form-label">{{ $textarea['name'] }}</label>
                    <textarea required name="answers[{{ $textarea['name'] }}]" class="form-control"
                              id="{{ $textarea['name'] }}"></textarea>
                    <div class="form-text">{{ $textarea['description'] }}</div>
                </div>
            @endforeach

            @foreach($form['inputs'] as $input)
                <div class="my-2">
                    <label for="{{ $input['name'] }}" class="form-label">{{ $input['name'] }}</label>
                    <input name="answers[{{ $input['name'] }}]" type="text" class="form-control"
                           id="{{ $input['name'] }}">
                    <div class="form-text">{{ $input['description'] }}</div>
                </div>
            @endforeach
            <div class="my-2">
                <button type="submit" class="btn btn-outline-primary">Submit</button>
            </div>
        </form>
    </div>

@endsection
