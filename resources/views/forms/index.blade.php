@extends('layouts.app')

@section('content')
    <table class="table table-success table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($forms as $form)
            <tr>
                <th scope="row">{{ $form['id'] }}</th>
                <td>{{ $form['name'] }}</td>
                <td class="d-flex">
                    <a class="btn btn-outline-primary" href="{{ route('forms.show', ['form' => $form['id']]) }}">Show answers</a>
                    <a class="btn btn-outline-success mx-2" href="{{ route('forms.fill', ['form' => $form['id']]) }}">Fill the form</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
